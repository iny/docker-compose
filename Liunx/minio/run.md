### MinIO

```shell
docker-compose -f docker-compose-minio.yml -p minio up -d
```

访问地址：[`ip地址:9000/minio`](http://www.zhengqingya.com:9000/minio)
登录账号密码：`root/password`